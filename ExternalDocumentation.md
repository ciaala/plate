# External Documentation


## Neo4j and GraphQL

- [GraphQL + Neo4j](https://neo4j.com/developer/graphql/)
- [Docker](https://docs.docker.com/samples/library/neo4j/)


## DB for prototyping

- [JSONServer, REST-API +in memory +persisted_file + json](https://github.com/typicode/json-server)


## UI

- [How to show the annotation](https://popper.js.org/)

## NLP

- https://kleiber.me/blog/2018/02/25/top-10-python-nlp-libraries-2018/